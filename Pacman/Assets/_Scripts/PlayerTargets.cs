﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargets : MonoBehaviour
{
    public Transform pinkysTargetLocation;
    public Transform inkysTargetLocation;

	EntityMovement playerMovement;

    void Start()
    {
        playerMovement = GetComponent<EntityMovement>();
    }

    void Update()
    {
        UpdatePinkysTargetLocation();
        UpdateInkysTargetLocation();
    }

    void UpdatePinkysTargetLocation()
    {
        switch(playerMovement.currentEntityDirection)
        {
            case(Direction.up):
                pinkysTargetLocation.position = this.transform.position + (Vector3.up * 5);
                break;
            case(Direction.down):
                pinkysTargetLocation.position = this.transform.position + (Vector3.down * 5);
                break;
            case(Direction.left):
                pinkysTargetLocation.position = this.transform.position + (Vector3.left * 5);
                break;
            case(Direction.right):
                pinkysTargetLocation.position = this.transform.position + (Vector3.right * 5);
                break;
            default:
                pinkysTargetLocation.position = this.transform.position;
                break;
        }
    }

    void UpdateInkysTargetLocation()
    {
        Transform blinkyTransform = GameManager.Instance.enemyManager.blinky.transform;
        Vector3 currentDirection = (blinkyTransform.position - pinkysTargetLocation.position);
        float distance = Vector3.Distance(blinkyTransform.position, pinkysTargetLocation.position);
        inkysTargetLocation.position = pinkysTargetLocation.position - (currentDirection.normalized * distance);
    }
}
