﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : EntityInput
{
    public override Direction ChosenDirection()
    {

        if(Input.GetButton("Up"))
        {
            return Direction.up;
        }
        else if(Input.GetButton("Down"))
        {
            return Direction.down;
        }
        else if(Input.GetButton("Left"))
        {
            return Direction.left;
        }
        else if(Input.GetButton("Right"))
        {
            return Direction.right;
        }

        return Direction.none;
    }

}

