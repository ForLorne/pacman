﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClydeBrain : GhostBrain
{
    float baseMovementSpeed = 5.0f;
    float frightenedMovementSpeed = 2.5f;
    float shyZone = 5.0f;

    void Start()
    {
        GameManager.Instance.enemyManager.ChaseModeEnableEvent += EnableChaseMode;
        GameManager.Instance.enemyManager.ScatterModeEnableEvent += EnableScatterMode;
        GameManager.Instance.enemyManager.FrightenedModeEnableEvent += EnableFrightenedMode;
        EnableScatterMode();
    }

    void Update()
    { 
        if(GameManager.Instance.enemyManager.activeGhostMode == GhostMode.Chase && target != null)
        {
            if(Vector3.Distance(this.transform.position, target.transform.position) < shyZone)
            {
                activeTarget = GameManager.Instance.enemyManager.clydeScatterTargetLocation;
            }
            else
            {
                activeTarget = target.transform;
            }
        }
    }
    
    protected override void CalculateTargetLocation()
    {
        base.CalculateTargetLocation();
    }

    protected override void EnableScatterMode()
    {
        activeMovementSpeed = baseMovementSpeed;
        activeTarget = GameManager.Instance.enemyManager.clydeScatterTargetLocation;
    }

    protected override void EnableChaseMode()
    {
        activeMovementSpeed = baseMovementSpeed;
        activeTarget = target.transform;
    }

    protected override void EnableFrightenedMode()
    {
        activeMovementSpeed = frightenedMovementSpeed;
    }
}
