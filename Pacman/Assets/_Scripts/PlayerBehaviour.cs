﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag.Equals("pellet"))
        {
            GameManager.Instance.playsessionManager.currentScore += 10;
            Destroy(other.gameObject);
        }

        if(other.tag.Equals("powerpellet"))
        {
            GameManager.Instance.playsessionManager.currentScore += 20;
            GameManager.Instance.enemyManager.FrightenedModeEnableEvent();
            Destroy(other.gameObject);
        }

        if(GameManager.Instance.enemyManager.activeGhostMode == GhostMode.Frighten && other.tag.Equals("ghost"))
        {
            other.gameObject.SetActive(false);
        }

        if(GameManager.Instance.enemyManager.activeGhostMode != GhostMode.Frighten && other.tag.Equals("ghost"))
        {
            GameManager.Instance.playsessionManager.GameOver();
        }
    }
}
