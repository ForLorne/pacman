﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaysessionManager : MonoBehaviour
{
	public int currentScore = 0;

    public Transform pellets;

    public GameObject gameOverText;
    public GameObject youWinText;

    void Update()
    {
        if(pellets.childCount <= 0)
        {
            GameManager.Instance.playsessionManager.YouWin();
        }
    }

    public void GameOver()
    {
        gameOverText.SetActive(true);
        Time.timeScale = 0;
    }

    public void YouWin()
    {
        youWinText.SetActive(true);
        Time.timeScale = 0;
    }
}
