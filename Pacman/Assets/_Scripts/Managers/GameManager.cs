﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
	public static GameManager Instance { get {return _instance; }}

    [HideInInspector]
    public EnemyManager enemyManager;
    public PlayerManager playerManager;
    public LevelManager levelManager;
    public PlaysessionManager playsessionManager;

    void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }

        enemyManager = FindObjectOfType<EnemyManager>();
        playerManager = FindObjectOfType<PlayerManager>();
        levelManager = FindObjectOfType<LevelManager>();
        playsessionManager = FindObjectOfType<PlaysessionManager>();

    }

}