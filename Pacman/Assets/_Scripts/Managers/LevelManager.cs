﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    Vector2 spawn = new Vector2(0.0f, -7.5f);

    public Transform blinkySpawn;
    public Transform pinkySpawn;
    public Transform inkySpawn;
    public Transform clydeSpawn;

    void Start()
    {
        GameObject pacman = Instantiate(GameManager.Instance.playerManager.pacman, spawn, Quaternion.identity);
        pacman.name = "Pacman";
    }

    
}
