﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject blinkySource;
    public GameObject pinkySource;
    public GameObject inkySource;
    public GameObject clydeSource;

	public Transform blinkyScatterTargetLocation;
    public Transform pinkyScatterTargetLocation;
    public Transform inkyScatterTargetLocation;
    public Transform clydeScatterTargetLocation;

    float frightenModeDuration = 10.0f;
    float frightenModeStart;

    [HideInInspector]
    public GhostMode activeGhostMode;
    [HideInInspector]
    public GameObject blinky;
    [HideInInspector]
    public GameObject pinky;
    [HideInInspector]
    public GameObject inky;
    [HideInInspector]
    public GameObject clyde;

    public delegate void ChaseMode();
    public ChaseMode ChaseModeEnableEvent;

    public delegate void ScatterMode();
    public ScatterMode ScatterModeEnableEvent;

    public delegate void FrightenedMode();
    public FrightenedMode FrightenedModeEnableEvent;

    void Start()
    {
        activeGhostMode = GhostMode.Scatter;
        ChaseModeEnableEvent += EnableChaseMode;
        ScatterModeEnableEvent += EnableScatterMode;
        FrightenedModeEnableEvent += EnableFrightenMode;

        blinky = Instantiate(blinkySource, GameManager.Instance.levelManager.blinkySpawn, false);
        pinky = Instantiate(pinkySource, GameManager.Instance.levelManager.pinkySpawn, false);
        inky = Instantiate(inkySource, GameManager.Instance.levelManager.inkySpawn, false);
        clyde = Instantiate(clydeSource, GameManager.Instance.levelManager.clydeSpawn, false);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Y)) ChaseModeEnableEvent();
        if(Input.GetKeyDown(KeyCode.U)) ScatterModeEnableEvent();
        if(Input.GetKeyDown(KeyCode.I)) FrightenedModeEnableEvent();

        if(Time.time - frightenModeStart > frightenModeDuration && activeGhostMode == GhostMode.Frighten)
        {
            RefreshGhosts();
            ChaseModeEnableEvent();
        }
  
    }

    void EnableChaseMode()
    {
        activeGhostMode = GhostMode.Chase;
    }

    void EnableScatterMode()
    {
        activeGhostMode = GhostMode.Scatter;
    }

    void EnableFrightenMode()
    {
        frightenModeStart = Time.time;
        activeGhostMode = GhostMode.Frighten;
    }

    void RefreshGhosts()
    {
        if(!blinky.activeSelf)
        {
            blinky.transform.position = GameManager.Instance.levelManager.blinkySpawn.position;
            blinky.SetActive(true);
        }

        if(!pinky.activeSelf)
        {
            pinky.transform.position = GameManager.Instance.levelManager.pinkySpawn.position;
            pinky.SetActive(true);
        }

        if(!inky.activeSelf)
        {
            inky.transform.position = GameManager.Instance.levelManager.inkySpawn.position;
            inky.SetActive(true);
        }

        if(!clyde.activeSelf)
        {
            clyde.transform.position = GameManager.Instance.levelManager.clydeSpawn.position;
            clyde.SetActive(true);
        }
            
    }


}

public enum GhostMode
{
    Chase, Scatter, Frighten
}
