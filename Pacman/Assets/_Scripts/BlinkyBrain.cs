﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyBrain : GhostBrain
{
    float baseMovementSpeed = 5.0f;
    float frightenedMovementSpeed = 2.5f;

    void Start()
    {
        GameManager.Instance.enemyManager.ChaseModeEnableEvent += EnableChaseMode;
        GameManager.Instance.enemyManager.ScatterModeEnableEvent += EnableScatterMode;
        GameManager.Instance.enemyManager.FrightenedModeEnableEvent += EnableFrightenedMode;
        EnableScatterMode();
    }
    
    protected override void CalculateTargetLocation()
    {
        base.CalculateTargetLocation();
    }

    protected override void EnableScatterMode()
    {
        activeMovementSpeed = baseMovementSpeed;
        activeTarget = GameManager.Instance.enemyManager.blinkyScatterTargetLocation;
    }

    protected override void EnableChaseMode()
    {
        activeMovementSpeed = baseMovementSpeed;
        activeTarget = target.transform;
    }

    protected override void EnableFrightenedMode()
    {
        activeMovementSpeed = frightenedMovementSpeed;
    }
    
}
