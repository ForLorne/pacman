﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EntityInput : MonoBehaviour
{
    public float activeMovementSpeed;

	public virtual Direction ChosenDirection()
    {
        return Direction.none;
    }

    public virtual Direction ChosenDirection(int[] clearDirections, Direction currentDirection, Transform location)
    {
        return Direction.none;
    }
}

public enum Direction
{
    up, down, left, right, none
}