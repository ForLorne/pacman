﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBrain : EntityInput
{
    protected GameObject target;
    protected Transform currentTargetLocation;
    protected Transform activeTarget;

    Vector2 upRouteCheckOffset = new Vector2(0.0f, 1.0f);
    Vector2 downRouteCheckOffset = new Vector2(0.0f, -1.0f);
    Vector2 leftRouteCheckOffset = new Vector2(-1.0f, 0.0f);
    Vector2 rightRouteCheckOffset = new Vector2(1.0f, 0.0f);

	public override Direction ChosenDirection(int[] clearDirections, Direction currentDirection, Transform location)
    {
        if(target == null)
        {
            GameObject pacmanInScene = GameObject.Find("Pacman");
            if(pacmanInScene != null) target = pacmanInScene;
            return Direction.none;
        }
        else
        {
            CalculateTargetLocation();
        }

        clearDirections = EliminateReverseDirection(clearDirections, currentDirection);
        Direction updatedDirection = CalculateShortestRouteToTarget(clearDirections, location, currentDirection);
        return updatedDirection;
    }


    int[] EliminateReverseDirection(int[] clearDirections, Direction currentDirection)
    {
        switch(currentDirection)
        {
            case(Direction.up):
                clearDirections[(int)Direction.down] = 1;
                break;
            case(Direction.down):
                clearDirections[(int)Direction.up] = 1;
                break;
            case(Direction.left):
                clearDirections[(int)Direction.right] = 1;
                break;
            case(Direction.right):
                clearDirections[(int)Direction.left] = 1;
                break;
            default:
                Debug.LogError("Invalid direction selected! @EliminateReverseDirection: " + currentDirection);
                break;
        }
        return clearDirections;
    }

    Direction CalculateShortestRouteToTarget(int[] clearDirections, Transform location, Direction currentDirection)
    {
        float shortestDistanceToTarget = Mathf.Infinity;
        float newDistanceToTarget = shortestDistanceToTarget;
        int winningDirection = -1;
        for(int i = 0; i < clearDirections.Length; i++)
        {
            if(clearDirections[i] == 0)
            {
                newDistanceToTarget = Vector3.Distance((location.position + OffsetLookup((Direction)i)), currentTargetLocation.position);
                if(newDistanceToTarget < shortestDistanceToTarget)
                {
                    shortestDistanceToTarget = newDistanceToTarget;
                    winningDirection = i;
                }
            }

        }
        return (Direction)winningDirection;
    }

    Vector3 OffsetLookup(Direction direction)
    {
        switch(direction)
        {
            case(Direction.up):
                return upRouteCheckOffset;
            case(Direction.down):
                return downRouteCheckOffset;
            case(Direction.left):
                return leftRouteCheckOffset;
            case(Direction.right):
                return rightRouteCheckOffset;
            default:
                return Vector2.zero;
        }
    }

    protected virtual void CalculateTargetLocation()
    {
        currentTargetLocation = activeTarget;
    }

    protected virtual void EnableScatterMode()
    {
        // target the corner
    }

    protected virtual void EnableChaseMode()
    {
        // swap back to standard chase.
    }

    protected virtual void EnableFrightenedMode()
    {
        // swap to frightened mode when power pellet is picked up.
    }
}