﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(RaycastController))]
public class EntityMovement : MonoBehaviour
{
    public Direction currentEntityDirection = Direction.down;
    public EntityInput entityInput;
    public bool isPlayer = false;

    float movementSpeed = 10.0f;
    Rigidbody2D entityRigidbody;
    RaycastController raycastController;
    Vector2 entityVelocity;

    void Start()
    {
        entityRigidbody = GetComponent<Rigidbody2D>();
        raycastController = GetComponent<RaycastController>();
    }

    void Update()
    {
        if(isPlayer) UpdatePlayerEntityVelocity(); 
            else UpdateGhostEntityVelocity();
    }

    void FixedUpdate()
    {
        entityRigidbody.velocity = entityVelocity * movementSpeed;
    }

    void UpdatePlayerEntityVelocity()
    {
        Direction targetEntityDirection = entityInput.ChosenDirection();
        if(targetEntityDirection == currentEntityDirection || targetEntityDirection == Direction.none) return;

        if(!raycastController.CheckSingleDirectionForObstacles(targetEntityDirection))
        {
            entityVelocity = DirectionToVector(targetEntityDirection);
            currentEntityDirection = targetEntityDirection;
        }
        else
        {
            entityVelocity = DirectionToVector(currentEntityDirection);
        }
    }

    void UpdateGhostEntityVelocity()
    {
        movementSpeed = entityInput.activeMovementSpeed;
        int[] obstacleReport = raycastController.CheckAllDirectionsForObstacles();
        Direction targetEntityDirection = entityInput.ChosenDirection(obstacleReport, currentEntityDirection, this.transform);
        if((int)targetEntityDirection == -1) targetEntityDirection = currentEntityDirection;
        if(targetEntityDirection == currentEntityDirection || targetEntityDirection == Direction.none) return;

        if(!raycastController.CheckSingleDirectionForObstacles(targetEntityDirection))
        {
            entityVelocity = DirectionToVector(targetEntityDirection);
            currentEntityDirection = targetEntityDirection;
        }
        else
        {
            entityVelocity = DirectionToVector(currentEntityDirection);
        }

    }

    Vector2 DirectionToVector(Direction direction)
    {
        switch (direction)
        {
            case Direction.up:
                return Vector2.up;
            case Direction.down:
                return Vector2.down;
            case Direction.left:
                return Vector2.left;
            case Direction.right:
                return Vector2.right;
            default:
                return Vector2.zero;
        }
    }

}
