﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBehaviour : MonoBehaviour
{
	SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(GameManager.Instance.enemyManager.activeGhostMode == GhostMode.Frighten)
        {
            spriteRenderer.color = new Color(255, 255, 255, 0.5f);
        }
        else
        {
            spriteRenderer.color = new Color(255, 255, 255, 1f);
        }

    }
}
