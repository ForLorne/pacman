﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastController : MonoBehaviour
{
	public float rayLength;
    public LayerMask collisionMask;

    float skinWidth = 0.015f;
    
    BoxCollider2D boxCollider;

    RaycastOrigins raycastOrigins;

    Vector2 firstRay;
    Vector2 secondRay;
    Vector2 rayDirection;


    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    void UpdateRaycastOrigins()
    {
        Bounds bounds = boxCollider.bounds;
        bounds.Expand(skinWidth * -2);

        raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
        raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
        raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
    }

    public bool CheckSingleDirectionForObstacles(Direction direction)
    {
        SetRayOriginsAndDirectionRelativeToTargetDirection(direction);

        RaycastHit2D firstHit = Physics2D.Raycast(firstRay, rayDirection, rayLength, collisionMask);
        Debug.DrawRay(firstRay, rayDirection * rayLength, Color.red, 2.0f);
        RaycastHit2D secondHit = Physics2D.Raycast(secondRay, rayDirection, rayLength, collisionMask);
        Debug.DrawRay(secondRay, rayDirection * rayLength, Color.red, 2.0f);

        return (firstHit || secondHit);
    }

    public int[] CheckAllDirectionsForObstacles()
    {
        int[] clearDirections = new int[] {0, 0, 0, 0};
        for(int i = 0; i < clearDirections.Length; i++)
        {
            SetRayOriginsAndDirectionRelativeToTargetDirection((Direction)i);
            if(CheckSingleDirectionForObstacles((Direction)i)) clearDirections[i] = 1;
        }

        // Debug.Log("UP: " + clearDirections[0] + " DOWN: " + clearDirections[1] + " LEFT: " + clearDirections[2] + " RIGHT: " + clearDirections[3]);
        
        return clearDirections;

    }

    void SetRayOriginsAndDirectionRelativeToTargetDirection(Direction direction)
    {
        UpdateRaycastOrigins();

        switch(direction)
        {
            case Direction.up:
                firstRay = raycastOrigins.topLeft;
                secondRay = raycastOrigins.topRight;
                rayDirection = Vector2.up;
                break;
            case Direction.down:
                firstRay = raycastOrigins.bottomLeft;
                secondRay = raycastOrigins.bottomRight;
                rayDirection = Vector2.down;
                break;
            case Direction.right:
                firstRay = raycastOrigins.topRight;
                secondRay = raycastOrigins.bottomRight;
                rayDirection = Vector2.right;
                break;
            case Direction.left:
                firstRay = raycastOrigins.topLeft;
                secondRay = raycastOrigins.bottomLeft;
                rayDirection = Vector2.left;
                break;
            default:
                Debug.LogWarning("Invalid direction selected!" + direction);
                break;
        }
    }

    struct RaycastOrigins 
    {
        public Vector2 topLeft;
        public Vector2 topRight;
        public Vector2 bottomLeft;
        public Vector2 bottomRight;
    }

}
