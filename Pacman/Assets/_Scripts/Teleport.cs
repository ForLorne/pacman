﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform teleportLocation;

    void OnTriggerEnter2D(Collider2D other)
    {
        other.transform.position = teleportLocation.position;
    }
}
